using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        GetComponentInChildren<Text>().text = string.Format("Timer: {0}:{1:00}", Mathf.Floor(timer/60), Mathf.Floor(timer%60));
    }
}
