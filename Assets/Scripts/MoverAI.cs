using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverAI : MonoBehaviour
{
    public enum State
    {
        Idle, Moving, Jumping
    }
    public State state = State.Idle;
    Vector3 direction = Vector3.zero;
    float jumpHeight = 1500;
    float timer = 0;
    public bool movingLeft = true;
    Collider c;
    Animator a;
    // Start is called before the first frame update
    void Start()
    {
        c = GetComponent<Collider>();
        a = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateStates();
        if (!CheckCollision(transform.position + direction * Time.deltaTime))
        {
            transform.Translate(direction * Time.deltaTime);
        }
        if (state == State.Jumping)
        {
            direction += Physics.gravity * (Time.deltaTime);
        }
        Vector3 viewPos = Camera.main.WorldToViewportPoint(transform.position);
        if (viewPos.x >= 0 && viewPos.x <= 1 && viewPos.y >= 0 && viewPos.y <= 1 && viewPos.z > 0)
        {
            timer += Time.deltaTime;
            if (timer >= 3)
            {
                timer = 0;
                Jump();
                if (movingLeft)
                {
                    MoveLeft();
                } else
                {
                    MoveRight();
                }
                a.SetBool("touchgrass", false);
            }
        }
    }

    void UpdateStates()
    {
        if (!Physics.Raycast(transform.position, Vector3.down, (transform.localScale.magnitude / 6) * 1.0001f) && state != State.Jumping)
        {
            state = State.Jumping;
            transform.parent = null;
        }
    }

    bool CheckCollision(Vector3 p)
    {
        bool onMovingPlatform = false;
        List<Collider> touching = new List<Collider>(Physics.OverlapBox(p, (transform.localScale / 6) * 1.0001f, transform.rotation, LayerMask.GetMask(new string[] { "Default" })));
        foreach (Collider l in touching)
        {
            if (l.CompareTag("MovingPlatform") && direction.y != 0)
            {
                print("coooll");
                transform.parent = l.transform;
                StopAllCoroutines();
                onMovingPlatform = true;
            }
            if (l.name.Equals("Ink(Clone)"))
            {
                Destroy(gameObject);
            }
            print(l.gameObject.name);
            Vector3 point = l.ClosestPointOnBounds(transform.position);
            Vector3 dir = (point - transform.position).normalized;
            print(dir);
            /*if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
            {
                //transform.Translate(new Vector3(-direction.x * 2, 0) * Time.deltaTime);
                //direction.x = 0;
            }*/
            if (Mathf.Abs(dir.y) > Mathf.Abs(dir.x))
            {
                //transform.Translate(new Vector3(0, -direction.y * 2) * Time.deltaTime);
                if (movingLeft)
                {
                    StopMovingLeft();
                } else
                {
                    StopMovingRight();
                }
                a.SetBool("touchgrass", true);
                direction.y = 0;
                if (state == State.Jumping)
                {
                    if (direction.x != 0)
                    {
                        state = State.Moving;
                    }
                    else
                    {
                        state = State.Idle;
                    }
                    break;
                }
            }
        }
        if (!onMovingPlatform)
        {
            
        }
        return touching.Count > 0;
    }

    public void MoveLeft()
    {
        state = State.Moving;
        direction.x = Random.Range(-1f, -10f);
    }

    public void MoveRight()
    {
        state = State.Moving;
        direction.x = 10;
    }

    public void StopMovingLeft()
    {
        if (direction.x < 0)
        {
            if (state != State.Jumping)
            {
                state = State.Idle;
            }
            direction.x = 0;
        }
    }

    public void StopMovingRight()
    {
        if (direction.x > 0)
        {
            if (state != State.Jumping)
            {
                state = State.Idle;
            }
            direction.x = 0;
        }
    }

    public void Jump()
    {
        state = State.Jumping;
        direction += Vector3.up * jumpHeight * 0.005f;
        transform.parent = null;
        a.SetTrigger("joomp");
        if (jumpHeight == 1500)
        {

        }
    }
    IEnumerator WaitAndRemoveParent()
    {
        yield return new WaitForSeconds(0.1f);
        transform.parent = null;
    }
}
