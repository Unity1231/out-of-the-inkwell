using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBetter : MonoBehaviour
{
    public enum WhatDoesItDo
    {
        JumpBetter, SpeedUp, SlowDown
    }
    public WhatDoesItDo whatDoesItDo = WhatDoesItDo.JumpBetter;
    private void OnEnable()
    {
        if (whatDoesItDo == WhatDoesItDo.JumpBetter)
        {
            //GameObject.Find("Koko").GetComponent<Mover>().jumpHeight += 500;
            GameObject.Find("Koko").GetComponent<Mover>().maxJumpCount += 1;
        } else if (whatDoesItDo == WhatDoesItDo.SpeedUp)
        {
            GameObject.Find("Koko").GetComponent<Mover>().moveSpeed *= (3f/2f);
            Invoke("SlowDown", 5f);
            enabled = false;
        } else
        {
            GameObject.Find("Koko").GetComponent<Mover>().moveSpeed *= (2f/3f);
            enabled = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SlowDown()
    {
        GameObject.Find("Koko").GetComponent<Mover>().moveSpeed *= (2f / 3f);
    }
}
