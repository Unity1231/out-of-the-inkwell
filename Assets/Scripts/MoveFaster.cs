using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MoveFaster : MonoBehaviour
{

    private void OnEnable()
    {
        GameObject.FindGameObjectsWithTag("Player").ToList().ForEach(delegate (GameObject g)
        {
            Vector3 viewPos = Camera.main.WorldToViewportPoint(g.transform.position);
            if (viewPos.x >= 0 && viewPos.x <= 1 && viewPos.y >= 0 && viewPos.y <= 1 && viewPos.z > 0)
                Destroy(g);
        });
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
