using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeAnimation : MonoBehaviour
{
    Mesh mesh;
    float direction = 1;
    float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        mesh.vertices[0].x += direction * Time.deltaTime;
        mesh.vertices[1].x -= direction * Time.deltaTime;
        if (timer >= 1)
        {
            direction = -direction;
        }
    }
}
