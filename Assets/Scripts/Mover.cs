using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public enum State
    {
        Idle, Moving, Jumping
    }
    public State state = State.Idle;
    Vector3 direction = Vector3.zero;
    public float jumpHeight = 1500;
    int jumpCount = 0;
    public int maxJumpCount = 1;
    public float moveSpeed = 1;
    Collider c;
    Animator a;
    // Start is called before the first frame update
    void Start()
    {
        c = GetComponent<Collider>();
        a = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateStates();
        a.SetFloat("speed", Mathf.Abs(direction.x / 10));
        GetComponentInChildren<SpriteRenderer>().flipX = direction.x < 0 ? true : (direction.x > 0 ? false : GetComponentInChildren<SpriteRenderer>().flipX);
        if (!CheckCollision(transform.position + direction * Time.deltaTime))
        {
            transform.Translate(direction * Time.deltaTime);
        }
        if (state == State.Jumping)
        {
            direction += Physics.gravity * (Time.deltaTime);
        }
        if (transform.position.y < -20)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
    }

    void UpdateStates()
    {
        if (!Physics.Raycast(transform.position, Vector3.down, (transform.localScale.magnitude / 2) * 1.0001f) && state != State.Jumping)
        {
            state = State.Jumping;
            transform.parent = null;
        }
    }

    bool CheckCollision(Vector3 p)
    {
        bool onMovingPlatform = false;
        List<Collider> touching = new List<Collider>(Physics.OverlapBox(p, (transform.localScale / 2) * 1.0001f, transform.rotation, LayerMask.GetMask(new string[] { "Default", "Player" })));
        foreach (Collider l in touching)
        {
            if (l.CompareTag("Player"))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
            } else if (l.CompareTag("Finish"))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
            }
            print(l.gameObject.name);
            Vector3 point = l.ClosestPointOnBounds(transform.position);
            Vector3 dir = (point - transform.position).normalized;
            print(dir);
            if (l.CompareTag("MovingPlatform"))
            {
                if (direction.y != 0)
                {
                    print("coooll");
                    transform.parent = l.transform;
                    onMovingPlatform = true;
                }
                else
                {
                    direction.x = -dir.normalized.x * 10;
                    StartCoroutine("WaitAndStopBounce");
                }
            }
            /*if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
            {
                //transform.Translate(new Vector3(-direction.x * 2, 0) * Time.deltaTime);
                //direction.x = 0;
            }*/
            if (Mathf.Abs(dir.y) > Mathf.Abs(dir.x))
            {
                //transform.Translate(new Vector3(0, -direction.y * 2) * Time.deltaTime);
                direction.y = 0;
                jumpCount = 0;
                if (state == State.Jumping)
                {
                    if (direction.x != 0)
                    {
                        state = State.Moving;
                    }
                    else
                    {
                        state = State.Idle;
                    }
                    break;
                }
            }
        }
        if (!onMovingPlatform)
        {
            
        }
        return touching.Count > 0;
    }

    public void MoveLeft()
    {
        state = State.Moving;
        direction.x = -10 * moveSpeed;
    }

    public void MoveRight()
    {
        state = State.Moving;
        direction.x = 10 * moveSpeed;
    }

    public void StopMovingLeft()
    {
        if (direction.x < 0)
        {
            if (state != State.Jumping)
            {
                state = State.Idle;
            }
            direction.x = 0;
        }
    }

    public void StopMovingRight()
    {
        if (direction.x > 0)
        {
            if (state != State.Jumping)
            {
                state = State.Idle;
            }
            direction.x = 0;
        }
    }

    public void Jump()
    {
        if (jumpCount < maxJumpCount)
        {
            state = State.Jumping;
            direction += Vector3.up * jumpHeight * 0.005f;
            transform.parent = null;
            jumpCount++;
            if (jumpHeight == 1500)
            {

            }
        }
    }
    IEnumerator WaitAndStopBounce()
    {
        yield return new WaitForSeconds(0.1f);
        direction.x = 0;
    }
}
