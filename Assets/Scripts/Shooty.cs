using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooty : MonoBehaviour
{
    public GameObject shootable;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Shoot()
    {
        GetComponentInChildren<Animator>().SetTrigger("shoot");
        StartCoroutine("Boom");
    }

    IEnumerator Boom()
    {
        for (int i = 0; i < 3; i++)
        {
            yield return new WaitForSeconds(0.1f);
            print("bang");
            Instantiate(shootable, transform.position + (Vector3.left / 2) + (Vector3.up / 3.5f), Quaternion.identity).GetComponent<Fly>().direction = !GetComponentInChildren<SpriteRenderer>().flipX;
        }
    }
}
