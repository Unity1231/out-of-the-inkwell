using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Time2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        float timer = FindObjectOfType<Timer>().timer;
        Destroy(FindObjectOfType<Timer>().gameObject);
        GetComponent<UnityEngine.UI.Text>().text = string.Format("Time: {0}:{1:00}", Mathf.Floor(timer / 60), Mathf.Floor(timer % 60));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
