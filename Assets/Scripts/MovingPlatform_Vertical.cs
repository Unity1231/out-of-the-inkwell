using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform_Vertical : MonoBehaviour
{
    float timer = 0;
    Vector3 pos;
    public float speed = 1;
    // Start is called before the first frame update
    void Start()
    {
        pos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        transform.Translate((Mathf.FloorToInt(timer) % 2 == 0 ? Vector3.down : Vector3.up) * Time.deltaTime * 10 * speed);
        Transform o = transform.Find("Koko");
        if (o != null)
        {
            o.localPosition = new Vector3(o.localPosition.x, 1.5f, o.localPosition.z);
        }
    }
}
