#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Xml;
using System.Linq;

public class ImportSparrow : EditorWindow
{
    Texture2D tex;
    TextAsset xml;
    Vector2 pivotPoint = new Vector2(0.5f, 0.5f);
    bool useAnimations = true;
    int frameRate = 24;
    string folder = "";
    [MenuItem("Tools/Import Sparrow Atlas")]
    static void GenerateSpritesheet()
    {
        CreateWindow<ImportSparrow>(new System.Type[] { });
    }
    private void OnGUI()
    {
        //folder = EditorGUILayout.TextField("Folder", folder, new GUILayoutOption[] { });
        tex = (Texture2D)EditorGUILayout.ObjectField("Spritesheet", tex, typeof(Texture2D), false);
        xml = (TextAsset)EditorGUILayout.ObjectField("XML file", xml, typeof(TextAsset), false);
        pivotPoint = EditorGUILayout.Vector2Field("Pivot Point", pivotPoint, new GUILayoutOption[] { });
        useAnimations = EditorGUILayout.Toggle("Use Animations", useAnimations, new GUILayoutOption[] { });
        GUI.enabled = useAnimations;
        frameRate = EditorGUILayout.IntField("Frame Rate", frameRate, new GUILayoutOption[] { });
        GUI.enabled = true;
        if (GUILayout.Button("Generate"))
        {
            Generate();
        }
    }

    void Generate()
    {
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xml.text);
        XmlNodeList list = doc.GetElementsByTagName("SubTexture");
        Dictionary<Sprite, string> sprites = new Dictionary<Sprite, string>();
        if (!AssetDatabase.IsValidFolder("Assets/" + folder))
        {
            AssetDatabase.CreateFolder("Assets/", folder);
        }
        AssetDatabase.CreateFolder("Assets/" + folder + "/", "Animations");
        AssetDatabase.CreateFolder("Assets/" + folder + "/", "Sprites");
        foreach (XmlNode cur in list)
        {
            try
            {
                AssetDatabase.DeleteAsset("Assets/" + cur.Attributes.GetNamedItem("name").Value + ".asset");
                Sprite s = Sprite.Create(tex, new Rect(int.Parse(cur.Attributes.GetNamedItem("x").Value), tex.height - (int.Parse(cur.Attributes.GetNamedItem("height").Value) + int.Parse(cur.Attributes.GetNamedItem("y").Value)), int.Parse(cur.Attributes.GetNamedItem("width").Value), int.Parse(cur.Attributes.GetNamedItem("height").Value)), pivotPoint, 100);
                AssetDatabase.CreateAsset(s, "Assets/" + cur.Attributes.GetNamedItem("name").Value + ".asset");
                sprites.Add(s, cur.Attributes.GetNamedItem("name").Value);
            } catch (System.Exception) {

            }
        }
        if (useAnimations)
        {
            Dictionary<string, KeyValuePair<AnimationClip, List<ObjectReferenceKeyframe>>> animations = new Dictionary<string, KeyValuePair<AnimationClip, List<ObjectReferenceKeyframe>>>();
            foreach (KeyValuePair<Sprite, string> s in sprites)
            {
                string val = s.Value.Remove(s.Value.Length - 4);
                if (!animations.ContainsKey(val))
                {
                    AnimationClip anim = new AnimationClip();
                    anim.frameRate = frameRate;
                    anim.name = val;
                    List<ObjectReferenceKeyframe> keys = new List<ObjectReferenceKeyframe>();
                    ObjectReferenceKeyframe o = new ObjectReferenceKeyframe();
                    o.time = 0;
                    o.value = s.Key;
                    keys.Add(o);
                    animations.Add(val, new KeyValuePair<AnimationClip, List<ObjectReferenceKeyframe>>(anim, keys));
                }
                else
                {
                    ObjectReferenceKeyframe o = new ObjectReferenceKeyframe();
                    o.time = (float)animations[val].Value.Count / frameRate;
                    o.value = s.Key;
                    animations[val].Value.Add(o);
                }
            }
            foreach (KeyValuePair<string, KeyValuePair<AnimationClip, List<ObjectReferenceKeyframe>>> a in animations)
            {
                AssetDatabase.DeleteAsset("Assets/" + a.Key + ".anim");
                AnimationClip clip = a.Value.Key;
                List<ObjectReferenceKeyframe> r = a.Value.Value;
                AnimationUtility.SetObjectReferenceCurve(clip, EditorCurveBinding.PPtrCurve("", typeof(SpriteRenderer), "m_Sprite"), r.ToArray());
                AssetDatabase.CreateAsset(clip, "Assets/" + a.Key + ".anim");
            }
        }
        AssetDatabase.SaveAssets();
    }
}
#endif